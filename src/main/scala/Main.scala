package fr.mipn.td

sealed trait Messages
case object Merci extends Messages
case class Addition(table: Int) extends Messages
case class Commande(boisson: String, quantite: Int, table: Int) extends Messages

def caisse(msgs: List[Messages]) = 
  val menu = Map(("café",1.5), ("thé", 2.0))
  var additions: Map[Int, Double] = Map().withDefaultValue(0.0)
  msgs.map (
    (msg: Messages) =>
    msg match
      case Merci => println("Merci à vous")
      case Addition(table) => 
        println(s"addition de la $table : ${additions(table)}")
        additions = additions.updated(table, 0.0)
      case Commande(boisson, quantite, table) => 
        additions += (table, additions(table) + menu.getOrElse(boisson, 0.0) * quantite)
  )
// def additions(les prix: ??, msgs : List[Messages]): List[??]


@main def hello(): Unit =
  println("Hello world!")
  println(msg)
  val msgs = List(Commande("café", 2, 3), 
  Commande("thé", 1, 3), 
  Commande("café", 1, 5), 
  Addition(3), 
  Merci, 
  Addition(5), 
  Commande("thé", 1, 3), 
  Addition(3))
  caisse(msgs)

def msg = "I was compiled by Scala 3. :)"
